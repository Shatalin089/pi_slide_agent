import os
import pathlib
import requests
from subprocess import PIPE, Popen, call

class SlideShowSibkassa(object):
    URL_GATE = 'http://185.60.133.130:3370'
    # URL_GATE = 'http://localhost:8000'

    ROUTE_LIST_IMG = '/slide/show/'

    HEADERS = {'Content-type': 'application/json',
               'Authorization': 'Token e4c5f10ec4a96a10f6c4bd184d657968a6b8a1d2'}

    def __init__(self, dir_file='~/'):
        self.dir_file = dir_file
        self.list_slide = self.__get_url_list()['list_image']

    def __get_url_list(self):
        res = requests.get(self.URL_GATE+self.ROUTE_LIST_IMG, headers=self.HEADERS)
        print(res.text)
        return res.json()


    def check_file(self, dir=None):
        if not dir:
            dir = self.dir_file

        for p in pathlib.Path(dir).iterdir():
            if p.is_file() and p.name in self.list_slide.keys():
                print('file {} is tut'.format(p))
                del self.list_slide[p.name]
            else:
                print('remove {}'.format(p))
                p.unlink()

    def get_files(self):
        for slide in self.list_slide:
            print(slide)
            self.get_file(slide, self.list_slide[slide]['url'])
        return True

    def get_file(self, name, url):
        res = requests.get(url=self.URL_GATE+'/'+url, headers=self.HEADERS)
        if res.status_code == 200:
            with open(self.dir_file+name, 'wb') as f:
                f.write(res.content)

    def kill_fbi(self):
        kill = call(['sudo', 'killall', 'fbi'])
        sdtart_fbi = call(['sudo', '/home/pi/pi_slide_agent/slideshow.sh'])

if __name__ == '__main__':
    sss = SlideShowSibkassa('/home/pi/slide/')
    sss.check_file()
    sss.get_files()
    sss.kill_fbi()